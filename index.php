<?php
    session_start();

    if(!isset($_SESSION["login"])){
        header("Location: login.php");
        exit;
    }

    include "config/controller.php";

    $karyawan = query("SELECT * FROM karyawan"); 

    if(isset($_POST["cari"])){
        $karyawan = cari($_POST["keyword"]);
    }
?>
<html>
    <head>
        <title>Belajar PHP</title>
        <style>
            .spinner{
                width:100px;
                position:absolute;
                top:113px;
                left:300px;
                z-index:-1;
                display:none;
            }
        </style>
        <script src="js/jquery-3.5.1.min.js"></script>
        <script src="js/script.js"></script>
    </head>
    <body>
        <a href="logout.php">LOGOUT</a>
        <h1>Daftar Karyawan</h1>
        <p><a href="create.php">Tambah Data Karyawan</a></p>
    
        <form action="" method="POST">
            <input type="text" name="keyword" size="40" placeholder="Masukkan Keyword" autocomplete="off" id="keyword">
            <button type="submit" name="cari" id="tombol-cari">Cari</button>
            <img src="img/spinner.gif" class="spinner">
        </form>
        <br>

        <div class="container" id="container">
            <table border="1" cellpadding="10" cellspacing="0">
                <tr>
                    <th>No</th>
                    <th>Gambar</th>
                    <th>Nama</th>
                    <th>Email</th>
                    <th>Posisi</th>
                    <th>Aksi</th>
                </tr>
                <?php 
                    $i = 1;
                    foreach($karyawan as $data):
                ?>
                <tr>
                    <td><?= @$i; ?></td>
                    <td><img src="img/<?= @$data ['gambar']; ?>" alt="" width="75px" heigth="75px"></td>
                    <td><?= @$data ['name']; ?></td>
                    <td><?= @$data ['email']; ?></td>
                    <td><?= @$data ['posisi']; ?></td>
                    <td>
                        <a onclick="return confirm('Apakah Anda Yakin ?')" href="hapus.php?hapus&id=<?= $data['id']?>" >HAPUS</a> |
                        <a href="ubah.php?id=<?= $data['id']?>">UBAH</a>

                    </td>
                </tr>
                <?php 
                    $i++;
                    endforeach;     
                ?>
            </table>
        </div>
    </body>
</html>