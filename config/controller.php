<?php 
    date_default_timezone_set("Asia/Jakarta");

    $username = "root";
    $password = "password";
    $database = "belajarphp";
    $hostname = "localhost";
    $con = mysqli_connect($hostname,$username,$password,$database) or die("Connection Corrupt");

    function query($query){
        global $con;
        $result = mysqli_query($con, $query);
        $rows = [];
        while($row = mysqli_fetch_assoc($result)){
            $rows[]= $row;
        }
        return $rows;
    }

    function tambah($data){
        global $con;

        $name = htmlspecialchars($data["name"]);
        $email = htmlspecialchars($data["email"]);
        $posisi = htmlspecialchars($data["posisi"]);


        $gambar = upload();
        if(!$gambar){
            return false;
        }

        $query = "INSERT INTO karyawan VALUES 
                ('', '$name', '$email', '$posisi', '$gambar')";
        mysqli_query($con, $query);

        return mysqli_affected_rows($con);
    }

    function upload(){
        $namaFile = $_FILES['gambar']['name'];
        $ukuranFile = $_FILES['gambar']['size'];
        $error = $_FILES['gambar']['error'];
        $tmpNama = $_FILES['gambar']['tmp_name'];

        if($error === 4){
            echo "<script>alert('Pilih gambar')</script>";
            return false;
        }

        $ekstensi = ['jpg','jpeg','png'];
        $ekstensiGambar = explode('.', $namaFile);
        $ekstensiGambar = strtolower(end($ekstensiGambar));

        if(!in_array($ekstensiGambar, $ekstensi)){
            echo "<script>alert('Format tidak mendukung')</script>";
            return false;
        }

        if($ukuranFile > 2000000){
            echo "<script>alert('Ukuran file terlalu besar')</script>";
            return false;
        }

        $newNamaFile  = uniqid();
        $newNamaFile .= '.';
        $newNamaFile .= $ekstensiGambar;

        move_uploaded_file($tmpNama, 'img/'. $newNamaFile);

        return $newNamaFile;
    }

    function hapus($id){
        global $con;

        mysqli_query($con, "DELETE FROM karyawan WHERE id = $id");
        return mysqli_affected_rows($con);
    }

    function ubah($data){
        global $con;

        $id = $data["id"];
        $name = htmlspecialchars($data["name"]);
        $email = htmlspecialchars($data["email"]);
        $posisi = htmlspecialchars($data["posisi"]);
        $gambarLama = htmlspecialchars($data["gambarLama"]);

        if($_FILES['gambar']['error'] === 4){
            $gambar = $gambarLama;
        }else{
            $gambar = upload();
        }

        $query = "UPDATE karyawan SET 
                  name = '$name', email = '$email', posisi = '$posisi', gambar = '$gambar' WHERE id = $id";
        mysqli_query($con, $query);

        return mysqli_affected_rows($con);
    }

    function cari($keyword){
        $query = "SELECT * FROM karyawan WHERE 
                  name LIKE '%$keyword%' OR
                  email LIKE '%$keyword%' OR
                  posisi LIKE '%$keyword%' 
                  ";

        return query($query);
    }

    function register($data){
        global $con;

        $name = htmlspecialchars($data["name"]);
        $username = strtolower(stripslashes($data["username"]));
        $email = htmlspecialchars($data["email"]);
        $password = mysqli_real_escape_string($con,$data["password"]);
        $password2 = mysqli_real_escape_string($con,$data["password2"]);

        $result = mysqli_query($con, "SELECT username, email FROM user WHERE username = '$username' OR email = '$email' ");
        if(mysqli_fetch_assoc($result)){
            echo "<script>alert('Username atau Email sudah terdaftar !');</script>";
            return false;
        }

        if($password !== $password2){
            echo "<script>alert('Konfirmasi password salah !')</script>";
            return false;
        }
        $password = password_hash($password, PASSWORD_DEFAULT);

        mysqli_query($con, "INSERT INTO user VALUES ('', '$name', '$username', '$email', '$password')");
        return mysqli_affected_rows($con);
    }

?>