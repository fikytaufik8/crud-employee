<?php 
    if(!isset($_GET["nama"]) || !isset($_GET["email"]) || !isset($_GET["posisi"])){
        header("Location: datakaryawan.php");
        exit;
    }
?>

<html>
    <head>
        <title></title>
    </head>
    <body>
        <ul>
            <li><?= $_GET["nama"]?></li>
            <li><?= $_GET["email"]?></li>
            <li><?= $_GET["posisi"]?></li>
        </ul>

        <a href="datakaryawan.php">Kembali</a>
    </body>
</html>