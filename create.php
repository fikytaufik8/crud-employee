<?php
    session_start();
    if(!isset($_SESSION["login"])){
        header("Location: login.php");
        exit;
    }

    include "config/controller.php";

    if(isset($_POST["submit"])){

        // var_dump($_POST);
        // var_dump($_FILES);die;

        if(tambah($_POST) > 0){
            echo "<script>alert('Sukses');
                  document.location.href='index.php'</script>
                 ";
        }else{
            echo"
                 <script>alert('Gagal');
                 document.location.href='create.php'</script>
                ";
        }
    }
?>
<html>
    <head>
        <title></title>
    </head>
    <body>
        <h1>Tambah Data Karyawan</h1>

        <form action="" method="POST" enctype="multipart/form-data">
            <ul>
                <li>
                    <label for="name">Nama</label>
                    <input type="text" name="name" id="name" placeholder="Nama Lengkap" required>
                </li>
                <li>
                    <label for="email">Email</label>
                    <input type="email" name="email" id="email" placeholder="Email" required>
                </li>
                <li>
                    <label for="posisi">Posisi</label>
                    <input type="text" name="posisi" id="posisi" placeholder="Posisi" required>
                </li>
                <li>
                    <label for="gambar">Gambar</label>
                    <input type="file" name="gambar" id="gambar">
                </li>
                <li>
                    <button type="submit" name="submit">Tambah Data</button>
                </li>
            </ul>
        </form>
    </body>
</html>