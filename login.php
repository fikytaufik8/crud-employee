<?php
    session_start();
    include "config/controller.php"; 
    
    if(isset($_COOKIE['id']) && isset($_COOKIE['key'])){
        $id = $_COOKIE['id'];
        $key = $_COOKIE['key'];

        $result = mysqli_query($con, "SELECT username FROM user WHERE id = $id");
        $row = mysqli_fetch_assoc($result);

        if($key === hash('sha256', $row['username'])){
            $_SESSION['login'] = true;
        }
    }
    // if(isset($_COOKIE['login'])){
    //     if($_COOKIE['login'] == 'true'){
    //         $_SESSION['login'] = true ;
    //     }
    // }

    if(isset($_SESSION["login"])){
        header("Location: index.php");
        exit;
    }

    if(isset($_POST["login"])){

        $username = $_POST["username"];
        $password = $_POST["password"];

        $result = mysqli_query($con, "SELECT * FROM user WHERE username = '$username'");
        
        if( mysqli_num_rows($result) === 1){
            
            $row = mysqli_fetch_assoc($result);
            
            if(password_verify($password, $row["password"])){
                
                $_SESSION["login"] = true;
                
                if(isset($_POST["remember"])){

                    setcookie('id', $row['id'], time() + 3600);
                    setcookie('key', hash('sha256',$row['username']), time() + 3600);
                }

                header("Location: index.php");
                exit;
            }
        }

        $error = true;
        // if($_POST["username"] == "Fiky Taufik" && $_POST["password"] == "terserah" ){
        //     header("Location: index.php");
        //     exit;
        // }else{
        //     $error = true;
        // }
    }
?>

<html>
    <head>
        <title>Login</title>
    </head>
    <body>
        <h1>Login Admin</h1>
        <?php if (isset($error)):?>
        <p>Username atau password salah !</p>
        <?php endif;?>
        <ul>
            <form action="" method="post">
                <li>
                    <label for="username">Username :</label>
                    <input type="text" name="username" placeholder="Username" id="username">
                </li>
                <li>
                    <label for="password">Password :</label>
                    <input type="password" name="password" placeholder="Password" id="password">
                </li>
                <li>
                    <input type="checkbox" name="remember" id="remember"> 
                    <label for="remember">Remember Me</label>
                </li>
                <li>
                    <button type="submit" name="login">Login</button>
                </li>
            </form>
        </ul>
    </body>
</html>