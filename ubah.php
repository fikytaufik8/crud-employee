<?php
    session_start();
    if(!isset($_SESSION["login"])){
        header("Location: login.php");
        exit;
    }
    include "config/controller.php";

    $id = $_GET["id"];
    $karyawan = query("SELECT * FROM karyawan WHERE id = $id")[0];


    if(isset($_POST["submit"])){

        

        if(ubah($_POST) > 0){
            echo "<script>alert('Sukses');
                  document.location.href='index.php'</script>
                 ";
        }else{
            echo"
                 <script>alert('Gagal');
                 document.location.href='index.php'</script>
                ";
        }
    }
?>
<html>
    <head>
        <title></title>
    </head>
    <body>
        <h1>Ubah Data Karyawan</h1>

        <form action="" method="POST" enctype="multipart/form-data">
            <input type="hidden" name="id" value="<?= $karyawan["id"]?>">
            <input type="hidden" name="gambarLama" value="<?= $karyawan["gambar"]?>">
            <ul>
                <li>
                    <label for="name">Nama</label>
                    <input type="text" name="name" id="name" value="<?= @$karyawan[name];?>" placeholder="Nama Lengkap" required>
                </li>
                <li>
                    <label for="email">Email</label>
                    <input type="email" name="email" id="email" value="<?= @$karyawan[email];?>" placeholder="Email" required>
                </li>
                <li>
                    <label for="posisi">Posisi</label>
                    <input type="text" name="posisi" id="posisi" value="<?= @$karyawan[posisi];?>" placeholder="Posisi" required>
                </li>
                <li>
                    <label for="gambar">Gambar</label>
                    <img src="img/<?= @$karyawan[gambar];?>" alt="">
                    <input type="file" name="gambar" id="gambar">
                </li>
                <li>
                    <button type="submit" name="submit">Ubah Data</button>
                </li>
            </ul>
        </form>
    </body>
</html>